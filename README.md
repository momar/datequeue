# DateQueue
> A Queue that automatically executes specific (async) functions at a specific time (rounded to the second).

![](https://teahub.ci.mo-mar.de/api/badges/momar/datequeue/status.svg)

You can find the full documentation at https://docs.mo-mar.de/datequeue/, or have a look at the tests to see some real-world examples.

**[Are you just looking for a simple queue without timing?](https://gist.github.com/moqmar/ad75424217ffe22882c7643a3c8a11d0)**

## Example usage

```javascript
const DateQueue = require("datequeue");
const dq = new DateQueue();

// q.push(date, func, prio, data)
q.push(new Date(Date.now() + 30000), function() {
    console.log("It's now 30 seconds later!");
}, 2);

// You can't use an arrow function if you want to access `this`.
let x = q.push(new Date(Date.now() + 30000), function() {
    // You can access the 4th argument using this.data
    console.log(this.data);
    // You can access all items accessed in a specific second ("timeframe") - in this case, there should be only the function itself!
    console.log(this.timeframe[0].id);
}, 0, "hello world");
```
