require("should");
const Suite = require("node-test");
const suite = new Suite("DateQueue");


const DateQueue = require("./datequeue");
const timer = function() { return function() { return Date.now() - this }.bind(Date.now()) };
const timeout = (n) => new Promise(r => setTimeout(() => r(), n));

suite.test("Immediate Execution", async function() {
    const x = timer();
    const q = new DateQueue();
    await q.push(new Date(), () => {}).promise;
    x().should.be.below(100);
});

suite.test("Timed Execution", async function() {
    const x = timer();
    const q = new DateQueue();
    await q.push(new Date(Date.now() + 3000), () => {}).promise;
    x().should.be.within(1900, 3100);
});

suite.test("Ordered Execution", async function() {
    const x = timer();
    const q = new DateQueue();
    let a = 0;
    await Promise.all([
        q.push(new Date(Date.now() + 3000), () => { x().should.be.within(2000, 3500); a.should.equal(2); a = 1; }).promise,
        // Test what happens if I add a task that should run *before* an existing task *after* that task has been processed.
        (async function() {
            await timeout(50);
            await q.push(new Date(Date.now() + 1000), () => { x().should.be.below(1500); a.should.equal(0); a = 2; }).promise
        })()
    ]);
    a.should.equal(1);
});

suite.test("Deleting Items", async function() {
    const q = new DateQueue();
    let a = 0;
    const items = [
        q.push(new Date(Date.now() + 3000), () => { a.should.equal(0); a = 1; }),
        q.push(new Date(Date.now() + 2000), () => { a = 2; throw new Error("Should be removed"); })
    ];
    q.remove(items[1]);
    await Promise.all([
        items[0].promise.should.be.fulfilled(),
        items[1].promise.should.be.rejected()
    ]);
    a.should.equal(1);
});

suite.test("Priorities", async function() {
    const q = new DateQueue();
    let a = 0;
    await Promise.all([
        q.push(new Date(Date.now() + 2000), () => { a.should.equal(3); a = 1; }).promise,
        // If the priority is the same, the order should be the same as when inserting:
        q.push(new Date(Date.now() + 2000), () => { a.should.equal(0); a = 2; }, 1).promise,
        q.push(new Date(Date.now() + 2000), () => { a.should.equal(2); a = 3; }, 1).promise
    ]);
    a.should.equal(1);
});

suite.test("Data Object", async function() {
    const q = new DateQueue();
    await q.push(new Date(), function () { this.data.should.equal("Hello World") }, 0, "Hello World").promise;
    await q.push(new Date(), function () { this.data.should.be.instanceof(Buffer)  }, 0, Buffer.alloc(1)).promise;
});

suite.test("Timeframes, Result & Status", async function() {
    const q = new DateQueue();
    const d = new Date();
    await Promise.all([
        q.push(d, () => 5, 0, "Hello").promise,
        q.push(d, function() {
            this.timeframe.length.should.equal(4);
            this.timeframe[1].data.should.equal("Hello");
            this.timeframe[1].result.should.equal(5);
            this.timeframe[0].status.should.equal(false);
            this.timeframe[0].result.message.should.equal("Whoa!");
            should(this.timeframe[2].status).equal(undefined);
            should(this.timeframe[3].status).equal(null);
        }).promise,
        q.push(d, function() { throw new Error("Whoa!")}, 2).promise.should.be.rejected(),
        q.push(d, function() {}).promise,
    ]);
});

suite.test("Simultaneous Execution", async function() {
    const x = timer();
    const q = new DateQueue(true);
    const d = new Date();

    await Promise.all([
        q.push(d, async function() { x().should.be.below(500); await timeout(1000) }, 10).promise,
        q.push(d, async function() { x().should.be.below(500); await timeout(1000) }, 10).promise,
        q.push(d, async function() { x().should.be.within(1000, 1500); await timeout(1000) }, 5).promise,
        q.push(d, async function() { x().should.be.within(1000, 1500); await timeout(1000) }, 5).promise,
        q.push(d, async function() { x().should.be.within(2000, 2500); await timeout(1000) }, 0).promise
    ]);

    x().should.be.within(3000, 3500);
});

suite.test("Sequential Execution", async function() {
    const x = timer();
    const q = new DateQueue(false);
    const d = new Date();


    const timeout = (n) => new Promise(r => setTimeout(() => r(), n));

    await Promise.all([
        q.push(d, async function() { x().should.be.below(500); await timeout(500) }, 10).promise,
        q.push(d, async function() { x().should.be.within(500, 1000); await timeout(500) }, 10).promise,
        q.push(d, async function() { x().should.be.within(1000, 1500); await timeout(500) }, 5).promise,
        q.push(d, async function() { x().should.be.within(1500, 2000); await timeout(500) }, 5).promise,
        q.push(d, async function() { x().should.be.within(2000, 2500); await timeout(500) }, 0).promise
    ]);

    x().should.be.within(2500, 3000);
});

suite.test("prepare() Function", async function() {
    const q = new DateQueue(false);
    const d = new Date();

    
    let i = 0;
    q.prepare = (timeframe) => i += timeframe.length;
    
    await q.push(d, () => i.should.equal(1)).promise;
    
    q.push(d, () => i.should.equal(3));
    await q.push(d, () => i.should.equal(3)).promise;
})
