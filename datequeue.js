/**
 * A Queue that automatically executes specific (async) functions at a specific time (rounded to the second).
 * @public
 */
class DateQueue {

    /**
     * Create a new, empty `DateQueue`.
     * @param {Boolean} [simultaneous=false] - If set to `true`, all functions in the same timeframe and with the same priority will be executed simultaneously.
     */
    constructor(simultaneous) {
        this.simultaneous = simultaneous;
        this.queue = {};
        this.working = false;
        this.id = 0;
        this.timeout = undefined;

        /**
         * If set to a function, will be executed before every timeframe, and receives the current timeframe as the first argument. `this` will be bound to the DateQueue object. Can be async.
         */
        this.prepare = null;
    }

    /**
     * Enqueue a function at a specified date.
     * @param {Date} date - The time on which `func` should be called
     * @param {Function} func - The function to call at `date`. Receives a `QueueItem` object as the `this` object.
     * @param {Number} [prio=0] - The priority of the function - the lower it is, the later in a timeframe a function is executed.
     * @param {Object} [data=null] - An object that is bound to `this.data` in `func`.
     * @returns {QueuePromise} Resolves/rejects like the function itself. Can also be used to remove the function from the `DateQueue`.
     */
    push(date, func, prio, data) {
        // Check if all the inputs are correct.
        if (typeof date !== "object" || typeof date.getTime !== "function") throw new TypeError("The first parameter must be a Date object");
        if (typeof func !== "function") throw new TypeError("The second parameter must be a Function");
        if (typeof prio !== "number" && typeof prio !== "undefined") throw new TypeError("The third parameter is optional, but must be a Number if given");
        if (typeof prio !== "number") prio = 0;

        // Create the queue for that timestamp if it doesn't exist yet.
        const timestamp = Math.floor(date.getTime() / 1000);
        const requiresReset = Object.keys(this.queue).sort()[0] > timestamp;
        if (typeof this.queue[timestamp] === "undefined") this.queue[timestamp] = [];
        
        // Find the last position with the same (or a higher) priority.
        let position = 0;
        for (let i = 0; i < this.queue[timestamp].length; i++) {
            if (this.queue[timestamp][i].prio >= prio) position = i+1;
            else break;
        }

        // Add the function to the queue at the position determined above.
        const item = new QueueItem({ prio, func, data, timestamp, id: this.id++ }, this);
        this.queue[timestamp].splice(position, 0, item);
        
        // Start processing the queue.
        if (!this.working) { this.working = true; this.timeout = setTimeout(() => this.work()); }
        else if (requiresReset) { clearTimeout(this.timeout); this.timeout = setTimeout(() => this.work()); }
        
        // Return the promise, so the application can wait for the function.
        return item;
    }

    /**
     * Remove an enqueued function from the queue.
     * @param {QueueItem} item - The `QueueItem` returned by `DateQueue.push()`
     * @returns {Boolean} `true` if the function has been removed, `false` if it couldn't be found in the queue.
     */
    remove(item) {
        // Check if all the inputs are correct.
        if (!(item instanceof QueueItem)) throw new TypeError("The parameter must be a QueueItem");
        if (typeof this.queue[item.timestamp] === "undefined") return false;

        // Find and remove the function by its ID.
        for (let i = 0; i < this.queue[item.timestamp].length; i++) {
            if (this.queue[item.timestamp][i].id == item.id) {
                this.queue[item.timestamp][i].reject(new Error("Removed"));
                this.queue[item.timestamp].splice(i, 1);
                if (Object.keys(this.queue).sort()[0] > item.timestamp) {
                    clearTimeout(this.timeout); this.timeout = setTimeout(() => this.work());
                }
                return true;
            }
        }

        // We didn't find anything.
        return false;
    }

    /**
     * Internal function to run the enqueued functions. <strong>Called automatically by `DateQueue.push()`</strong>
     * @private
     */
    async work() {
        this.working = true;

        let timestamps = Object.keys(this.queue).sort();
        let now = Date.now();
        
        // Go through all timestamps that are already over.
        for (let i = 0; timestamps[i] * 1000 <= now; i++) {
            const timeframe = this.queue[timestamps[i]].splice(0, this.queue[timestamps[i]].length);
            const timeframeSnapshot = timeframe.slice();
            
            if (typeof this.prepare === "function") await this.prepare.call(this, timeframeSnapshot);
            
            if (this.simultaneous === true) {

                // Create array with functions of the highest priority, for simultaneous execution.
                let tasks = [];
                let prio = null;

                for (let i = 0; i <= timeframeSnapshot.length; i++) {
                    if (timeframe.length <= 0 || timeframe[0].prio !== prio) {
                        if (prio !== null) {

                            // Execute all functions with the same priority simultaneously.
                            await Promise.all(tasks.map(item => item.exec(timeframeSnapshot)));

                        }
                        tasks = [];
                        if (timeframe.length <= 0) continue; // no items left to process
                        else prio = timeframe[0].prio;    
                    }

                    if (timeframe.length > 0) {
                        tasks.push(timeframe.shift());
                    }
                }

            } else {
                
                while (timeframe.length > 0) {
                    // Execute the function, and resolve or reject the promise.
                    await timeframe.shift().exec(timeframeSnapshot);
                }

            }
            // Everything is empty now, so we delete the queue for that timestamp.
            if (this.queue[timestamps[i]].length === 0) delete this.queue[timestamps[i]];
        }

        // We're done, so we see if there are new timestamps and set a timeout to the next one.
        let timeout = 0;
        timestamps = Object.keys(this.queue).sort();
        now = Date.now();
        if (timestamps.length > 0) {
            // If the first timestamp is already over, we run work() again right now (hence timeout = 0 by default).
            if (timestamps[0] * 1000 > now) timeout = timestamps[0] * 1000 - now;
            this.timeout = setTimeout(() => this.work(), timeout);
        } else {
            this.working = false;
        }
    }
}

/**
 * Represents an item in a `DateQueue`. Keeps some metadata and a `Promise` that can be resolved and rejected externally.
 * @inner
 * @memberof DateQueue
 * @protected
 */
class QueueItem {
    /**
     * Create a new `QueueItem`. <strong>Called automatically by `DateQueue.push()`</strong>
     * @param {Object} data - An object containing the required metadata of the item: `{ func, data, prio, timestamp, id }`
     * @param {DateQueue} queue - A reference to the queue itself.
     * @private
     */
    constructor(data, queue) {

        /**
         * The promise that will be rejected/resolved if the function has been executed.
         * @readonly
         */
        this.promise = new Promise((res, rej) => [this.resolve, this.reject] = [res, rej]);

        /**
         * A reference to the `DateQueue` this item is part of.
         * @readonly
         */
        this.queue = queue;
        
        /**
         * The actual function to run at the specified time.
         */
        this.func = data.func.bind(this);

        /**
         * The `data` object from `DateQueue.push()`.
         */
        this.data = data.data;
        
        /**
         * The priority of the item, as set by the user.
         * @readonly
         */
        this.prio = data.prio || 0;

        /**
         * The timestamp on which the item will be run.
         * @readonly
         */
        this.timestamp = data.timestamp;
        
        /**
         * The incremental ID of the item, used for removal of items.
         * @readonly
         */
        this.id = data.id;

        /**
         * Has the function been executed, and did it succeed?
         * 
         * Possible values:
         * 
         * - `null`: not yet started
         * 
         * - `undefined`: currently running
         * 
         * - `true`: executed successfully, the return value is stored in `result`
         * 
         * - `false`: execution failed, the error is stored in `result`
         * @readonly
         */
        this.status = null;

        /**
         * The return value or error (depends on `status`) of the function.
         * @readonly
         */
        this.result = undefined;
    }

    /**
     * Call the function of the `QueueItem`. <strong>Called automatically by `DateQueue.push()`</strong>
     * @param {Array} timeframe The array of `QueueItem` objects that are executed in the same timeframe.
     * @private
     */
    async exec(timeframe) {
        this.timeframe = timeframe;
        try {
            this.status = undefined;
            let r = await this.func();
            this.status = true;
            this.result = r;
            this.resolve(r);
        } catch (ex) {
            this.status = false;
            this.result = ex;
            this.reject(ex);
        }
    }
}

module.exports = DateQueue;
module.exports.QueueItem = QueueItem;
